package com.example.demo.service;

import com.example.demo.entity.Product;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository repo;

    public Product saveProduct(Product product){
        return repo.save(product);
    }
    public List<Product> saveProducts(List<Product> product){
        return repo.saveAll(product);
    }
    public List<Product> getProducts(){
        return repo.findAll();
    }
    public Product getProductsById(int Id){
        return repo.findById(Id).orElse(null);
    }
    public Product getProductsByName(String name){
        return repo.findByName(name);
    }
    public String deleteProduct(Integer Id){
        repo.deleteById(Id);
        return "Product remover!!";
    }
//    public Product updateProduk(Product produk){
//        Product existingProduk = repo.findById(produk.getId()).orElse(null);
//        existingProduk.setName(produk.getName());
//        existingProduk.setQyt(produk.getQYT());
//        existingProduk.setPrice(produk.getPrice());
//        return repo.save(existingProduk);
//    }

}

